﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testAppForRc.DAL;
using testAppForRc.Models;
using testAppForRc.Models.Domain;
using testAppForRc.Models.View.UserViewModels;
using testAppForRc.Repository.UserRepository;

namespace testAppForRc.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<ActionResult> Index(IndexUserViewModel model)
        {
            model.Items = await _userRepository.SelectListAsync();

            return View("Index", model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var user = await _userRepository.SelectAsync(id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                     Email = model.Email,
                     Password = model.Password,
                     Age = model.Age,
                     FirstName = model.FirstName,
                     LastName = model.LastName,
                };

                await _userRepository.InsertAsync(user);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var user = await _userRepository.SelectAsync(id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }

            var model = new EditUserViewModel
            {
                Id = user.UserId,
                Age = user.Age,
                Email = user.Email,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
            };

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userRepository.SelectAsync(model.Id);

                user.Age = model.Age;
                user.Email = model.Email;
                user.Password = model.Password;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;

                await _userRepository.UpdateAsync(user);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var user = await _userRepository.SelectAsync(id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }

            var model = new IndexUserViewModel
            {
                Id = user.UserId,
                LastName = user.LastName,
                FirstName = user.FirstName,
                Age = user.Age,
                Email = user.Email
            };

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _userRepository.DeleteAsync(id);
            return RedirectToAction("Index");
        }

    }
}
