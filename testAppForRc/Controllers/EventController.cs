﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using testAppForRc.DAL;
using testAppForRc.Models.Domain;
using testAppForRc.Models.View;
using testAppForRc.Models.View.EventViewModels;
using testAppForRc.Models.View.UserViewModels;
using testAppForRc.Repository.EventRepository;
using testAppForRc.Repository.UserRepository;

namespace testAppForRc.Controllers
{
    public class EventController : Controller
    {
        private readonly IEventRepository _eventRepository;
        private readonly IUserRepository _userRepository;

        public EventController(IEventRepository eventRepository, IUserRepository userRepository)
        {
            _eventRepository = eventRepository;
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<ActionResult> Index(IndexEventViewModel model)
        {
            model.Events = await _eventRepository.SelectListAsync();

            return View("Index", model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var @event = await _eventRepository.SelectAsync(id.Value);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var users = await _userRepository.SelectListAsync();
            var viewModel = new List<AssignedUserData>();
            foreach (var user in users)
            {
                viewModel.Add(new AssignedUserData
                {
                    UserId = user.UserId,
                    FullName = user.LastName + " " + user.FirstName,
                    Assigned = false,
                });
            }
            ViewBag.Users = viewModel;

            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateEventViewModel model, string[] selectedUsers)
        {
            if (ModelState.IsValid)
            {
                var date = DateTime.ParseExact(model.EvetDateString, "HH:mm, dd-MM-yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);

                var @event = new Event
                {
                    Name = model.Name,
                    EvetDate = date,
                    Place = model.Place,
                };

                await _eventRepository.InsertAsync(@event, selectedUsers);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }
            var @event = await _eventRepository.SelectAsync(id.Value);

            await PopulateAssignedUserData(@event);

            if (@event == null)
            {
                return HttpNotFound();
            }

            var model = new EditEventViewModel
            {
                EventId = @event.EventId,
                Name = @event.Name,
                EvetDateString = @event.EvetDate.ToString("HH:mm, dd-MM-yyyy"),
                Place = @event.Place,
                Users = @event.Users
            };

            return View(model);
        }

        private async Task PopulateAssignedUserData(Event @event)
        {
            var allUsers = await _userRepository.SelectListAsync();
            var eventUsers = @event.Users;
            var viewModel = new List<AssignedUserData>();
            foreach (var user in allUsers)
            {
                var element = eventUsers.FirstOrDefault(i => i.UserId == user.UserId);
                var assigned = element != null;
                viewModel.Add(new AssignedUserData
                {
                    UserId = user.UserId,
                    FullName = user.LastName + " " + user.FirstName,
                    Assigned = assigned
                });
            }
            ViewBag.Users = viewModel;
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditEventViewModel model, string[] selectedUsers)
        {
            if (ModelState.IsValid)
            {
                var @event = await _eventRepository.SelectAsync(model.EventId);

                var date = DateTime.ParseExact(model.EvetDateString, "HH:mm, dd-MM-yyyy",
                    System.Globalization.CultureInfo.InvariantCulture);

                @event.Name = model.Name;
                @event.EvetDate = date;
                @event.Place = model.Place;

                await _eventRepository.UpdateAsync(@event, selectedUsers);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return View("Error");
            }

            var @event = await _eventRepository.SelectAsync(id.Value);
            if (@event == null)
            {
                return HttpNotFound();
            }

            var model = new EditEventViewModel
            {
                EventId = @event.EventId,
                Name = @event.Name,
                EvetDateString = @event.EvetDate.ToString("HH:mm, dd MMM yyyy"),
                Place = @event.Place,
                Users = @event.Users
            };

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _eventRepository.DeleteAsync(id);
            return RedirectToAction("Index");
        }


    }
}
