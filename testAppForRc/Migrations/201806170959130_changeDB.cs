namespace testAppForRc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeDB : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Event", "Name", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Event", "Place", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Event", "Place", c => c.String());
            AlterColumn("dbo.Event", "Name", c => c.String());
        }
    }
}
