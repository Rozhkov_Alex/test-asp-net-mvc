namespace testAppForRc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructDatabase : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UE", "EventId", "dbo.Event");
            DropForeignKey("dbo.UE", "UserId", "dbo.User");
            DropIndex("dbo.UE", new[] { "UserId" });
            DropIndex("dbo.UE", new[] { "EventId" });
            DropPrimaryKey("dbo.User");
            CreateTable(
                "dbo.EventUser",
                c => new
                    {
                        EventId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventId, t.UserId })
                .ForeignKey("dbo.Event", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.UserId);

            DropColumn("dbo.User", "Id");
            AddColumn("dbo.User", "UserId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.User", "UserId");
            
            DropTable("dbo.UE");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UE",
                c => new
                    {
                        UeId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UeId);
            
            AddColumn("dbo.User", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.EventUser", "UserId", "dbo.User");
            DropForeignKey("dbo.EventUser", "EventId", "dbo.Event");
            DropIndex("dbo.EventUser", new[] { "UserId" });
            DropIndex("dbo.EventUser", new[] { "EventId" });
            DropPrimaryKey("dbo.User");
            DropColumn("dbo.User", "UserId");
            DropTable("dbo.EventUser");
            AddPrimaryKey("dbo.User", "Id");
            CreateIndex("dbo.UE", "EventId");
            CreateIndex("dbo.UE", "UserId");
            AddForeignKey("dbo.UE", "UserId", "dbo.User", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UE", "EventId", "dbo.Event", "EventId", cascadeDelete: true);
        }
    }
}
