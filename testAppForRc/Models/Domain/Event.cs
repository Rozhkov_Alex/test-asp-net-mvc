﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace testAppForRc.Models.Domain
{
    public class Event
    {
        public int EventId { get; set; }

        [Required(ErrorMessage = "Введите название")]
        [DisplayName("Название")]
        [StringLength(64, ErrorMessage = "Кол-во символов не должно превышать 64")]
        public string Name { get; set; }

        [DisplayName("Место проведения")]
        [StringLength(128, ErrorMessage = "Кол-во символов не должно превышать 128")]
        public string Place { get; set; }

        [Required(ErrorMessage = "Введите время и дату")]
        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:hh:mm, dd MMM yyyy}")]
        public DateTime EvetDate { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}