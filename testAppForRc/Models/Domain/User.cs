﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace testAppForRc.Models.Domain
{
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public virtual ICollection<Event> Events { get; set; }
    }
}