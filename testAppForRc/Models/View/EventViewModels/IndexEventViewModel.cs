﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testAppForRc.Models.Domain;

namespace testAppForRc.Models.View.EventViewModels
{
    public class IndexEventViewModel
    {
        public IEnumerable<Event> Events { get; set; }
        //public IEnumerable<User> Users { get; set; }
    }
}