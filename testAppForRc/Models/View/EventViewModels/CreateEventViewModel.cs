﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using testAppForRc.Models.Domain;

namespace testAppForRc.Models.View.EventViewModels
{
    public class CreateEventViewModel
    {
        [Required(ErrorMessage = "Введите название")]
        [DisplayName("Название")]
        [StringLength(64, ErrorMessage = "Кол-во символов не должно превышать 64")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите время и дату")]
        [DisplayName("Дата")]
        [DataType(DataType.Text)]
        public string EvetDateString { get; set; }

        [DisplayName("Место проведения")]
        [StringLength(128, ErrorMessage = "Кол-во символов не должно превышать 128")]
        public string Place { get; set; }

        //[Required(ErrorMessage = "Введите хотя бы одного пользователя")]
        //[DisplayName("Участники события")]
        //public virtual ICollection<User> Users { get; set; }
    }
}