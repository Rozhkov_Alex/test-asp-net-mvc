﻿using System.Collections.Generic;

namespace testAppForRc.Models.View
{
    public class BaseIndexViewModel<T>
    {
        public List<T> Items { get; set; }
    }
}