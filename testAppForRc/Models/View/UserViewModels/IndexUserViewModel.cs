﻿using System.ComponentModel;
using testAppForRc.Models.Domain;

namespace testAppForRc.Models.View.UserViewModels
{
    public class IndexUserViewModel : BaseIndexViewModel<User>
    {
        public int Id { get; set; }

        [DisplayName("Почта")]
        public string Email { get; set; }

        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [DisplayName("Фамилия")]
        public string LastName { get; set; }

        [DisplayName("Возраст")]
        public int Age { get; set; }

        [DisplayName("Кол-во событий")]
        public int CountEvent { get; set; }
    }
}