﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace testAppForRc.Models.View.UserViewModels
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "Укажите почту")]
        [DisplayName("Почта")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Укажите пароль")]
        [DisplayName("Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Укажите имя")]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Укажите фамилию")]
        [DisplayName("Фамилия")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Укажите возраст")]
        [DisplayName("Возраст")]
        public int Age { get; set; }
    }
}