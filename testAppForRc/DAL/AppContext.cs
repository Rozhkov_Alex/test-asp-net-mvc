﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using testAppForRc.Models.Domain;

namespace testAppForRc.DAL
{
    public class AppContext : DbContext
    {
        public AppContext() : base("DefaultConnection")
        {
            //Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<User> Users { get; set; }
        //public DbSet<UE> Ues { get; set; }
        public DbSet<Event> Events { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Event>()
                .HasMany(c => c.Users).WithMany(i => i.Events)
                .Map(t => t.MapLeftKey("EventId")
                .MapRightKey("UserId")
                .ToTable("EventUser"));
        }
    }
}