﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using testAppForRc.DAL;
using testAppForRc.Models.Domain;
using testAppForRc.Repository;

namespace testAppForRc.Repository.UserRepository
{
    public class UserRepository : IUserRepository
    {
        public async Task<List<User>> SelectListAsync()
        {
            List<User> result;
            using (var db = new AppContext())
            {
                result = await db.Users
                    .Include(i => i.Events)
                    .ToListAsync();
            }
            return result;
        }

        public async Task<User> SelectAsync(int id)
        {
            User result;
            using (var db = new AppContext())
            {
                var list = await db.Users
                    .Include(i => i.Events)
                    .ToArrayAsync();

                result = list.FirstOrDefault(user => user.UserId == id);
            }
            return result;
        }

        public async Task InsertAsync(User user)
        {
            using (var db = new AppContext())
            {
                db.Users.Add(user);
                await db.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(User user)
        {
            using (var db = new AppContext())
            {
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var db = new AppContext())
            {
                User user = await db.Users.FindAsync(id);
                db.Users.Remove(user);
                await db.SaveChangesAsync();
            }
        }
    }
}