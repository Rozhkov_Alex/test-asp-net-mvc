﻿using System.Collections.Generic;
using System.Threading.Tasks;
using testAppForRc.Models.Domain;

namespace testAppForRc.Repository.UserRepository
{
    public interface IUserRepository
    {
        Task<List<User>> SelectListAsync();
        Task<User> SelectAsync(int id);
        Task InsertAsync(User user);
        Task UpdateAsync(User user);
        Task DeleteAsync(int id);
    }
}