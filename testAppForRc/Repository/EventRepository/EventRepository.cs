﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using testAppForRc.DAL;
using testAppForRc.Models.Domain;

namespace testAppForRc.Repository.EventRepository
{
    public class EventRepository : IEventRepository
    {
        public async Task<List<Event>> SelectListAsync()
        {
            List<Event> result;
            using (var db = new AppContext())
            {
                result = await db.Events
                    .Include(i => i.Users)
                    .ToListAsync();
            }
            return result;
        }

        public async Task<Event> SelectAsync(int id)
        {
            Event result;
            using (var db = new AppContext())
            {
                var list = await db.Events
                    .Include(i => i.Users)
                    .ToArrayAsync();

                result = list.FirstOrDefault(ev => ev.EventId == id);
            }
            return result;
        }

        public async Task InsertAsync(Event @event, string[] selectedUsers)
        {
            using (var db = new AppContext())
            {
                if (selectedUsers != null)
                {
                    var allUsers = db.Users.ToList();
                    @event.Users = new List<User>();

                    foreach (var user in allUsers)
                    {
                        int index = Array.IndexOf(selectedUsers, user.UserId.ToString());
                        var assigned = index != -1;

                        if (assigned)
                        {
                            @event.Users.Add(user);
                        }

                    }
                }
                db.Events.Add(@event);
                await db.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(Event @event, string[] selectedUsers)
        {
            using (var db = new AppContext())
            {
                var ev = db.Events.FirstOrDefault(i => i.EventId == @event.EventId);

                if (selectedUsers != null)
                {
                    var allUsers = db.Users.ToList();

                    if (ev != null)
                    {
                        ev.Users = new List<User>();
                        ev.Users.Clear();

                        foreach (var user in allUsers)
                        {
                            int index = Array.IndexOf(selectedUsers, user.UserId.ToString());
                            var assigned = index != -1;

                            if (assigned)
                            {
                                ev.Users.Add(user);
                            }

                        }
                    }
                }
                else
                {
                    ev?.Users.Clear();
                }

                db.Entry(ev).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var db = new AppContext())
            {
                Event @event = await db.Events.FindAsync(id);
                db.Events.Remove(@event);
                await db.SaveChangesAsync();
            }
        }
    }
}