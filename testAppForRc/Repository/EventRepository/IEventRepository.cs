﻿using System.Collections.Generic;
using System.Threading.Tasks;
using testAppForRc.Models.Domain;

namespace testAppForRc.Repository.EventRepository
{
    public interface IEventRepository
    {
        Task<List<Event>> SelectListAsync();
        Task<Event> SelectAsync(int id);
        Task InsertAsync(Event @event, string[] selectedUsers);
        Task UpdateAsync(Event @event, string[] selectedUsers);
        Task DeleteAsync(int id);
    }
}