﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(testAppForRc.Startup))]
namespace testAppForRc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
