﻿
var months = ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"];

function sortTable(nameTable, columnNumber, isDate, inversion) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById(nameTable);
    switching = true;

    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName("TD")[columnNumber];
            y = rows[i + 1].getElementsByTagName("TD")[columnNumber];
            if (inversion)
            {
                if (!isDate) {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else {
                    var re = /\,|\s|\:/;
                    //удаление лишних символов
                    var line = x.innerText.replace(/^\s+|\s+$/g, '');
                    var p = line.split(re);
                    p[4] = getMonthNumber(p[4]);
                    var date1 = new Date(p[5], p[4], p[3], p[0], p[1]);

                    //удаление лишних символов
                    var linee = y.innerText.replace(/^\s+|\s+$/g, '');
                    var f = linee.split(re);
                    f[4] = getMonthNumber(f[4]);
                    var date2 = new Date(f[5], f[4], f[3], f[0], f[1]);
                    if (date1 < date2) {
                        shouldSwitch = true;
                        break;
                    }
                }
            } else {
                if (!isDate)
                {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else
                {
                    var rex = /\,|\s|\:/;
                    //удаление лишних символов
                    var line1 = x.innerText.replace(/^\s+|\s+$/g,'');
                    var p1 = line1.split(rex);
                    p1[4] = getMonthNumber(p1[4]);
                    var date11 = new Date(p1[5], p1[4], p1[3], p1[0], p1[1]);

                    //удаление лишних символов
                    var line2 = y.innerText.replace(/^\s+|\s+$/g, '');
                    var f1 = line2.split(rex);
                    f1[4] = getMonthNumber(f1[4]);
                    var date22 = new Date(f1[5], f1[4], f1[3], f1[0], f1[1]);
                    if (date11 > date22) {
                        shouldSwitch = true;
                        break;
                    }
                }
                
            }

            
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}


function getMonthNumber(month) {

    for (var i = 0; i < months.length; i++) {
        if (month == months[i]) {
            return  i;
        }
    }

}


