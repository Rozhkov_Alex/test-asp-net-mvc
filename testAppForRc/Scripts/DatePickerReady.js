﻿$(function () {
    $('#datetimepicker').datetimepicker({
        format: 'HH:mm, DD-MM-YYYY',
        locale: 'ru'
    });
});